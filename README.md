Jenkins docker image with preinstalled build tools

List of installed build tools:
- Node 11
- Yarn 1.13.0
- Dotnet SDK 2.2


Build image
```
docker build --force-rm -t bluexd/jenkins:1.1.0 .
```

Run image
```
docker run -p 8080:8080 -v your_machine/jenkins_home:/var/jenkins_home bluexd/jenkins:1.0.0
```
Note that 50000 is not exposed. Taken from the documentation for the image that this one is based on:

"You can run builds on the master out of the box.

But if you want to attach build slave servers through JNLP (Java Web Start): make sure you map the port: -p 50000:50000 - which will be used when you connect a slave agent."

Jenkins plugins recommened plugins
- Git
- Pipeline
- Pipeline: Multibranch
- Bitbucket Branch Source

For setup with bitbucket \
https://go.cloudbees.com/docs/plugins/bitbucket/

And for automatic trigger add webhook to <JENKINS-URL>/bitbucket-scmsource-hook/notify \
https://support.cloudbees.com/hc/en-us/articles/115000053051-How-to-Trigger-Multibranch-Jobs-from-Bitbucket-Server-

