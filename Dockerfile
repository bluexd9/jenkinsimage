FROM jenkins/jenkins:2.166
USER root

# Install NodeJs
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash -
RUN apt-get install -y nodejs

# Install Yarn
COPY yarn-v1.13.0.tar.gz /opt
RUN tar zvxf /opt/yarn-v1.13.0.tar.gz -C /opt
ENV PATH "$PATH:/opt/yarn-v1.13.0/bin"

# Install .Net Core SDK
COPY dotnet-install.sh .
RUN ./dotnet-install.sh --channel 2.2 --install-dir /opt/dotnet
ENV PATH "$PATH:/opt/dotnet"

USER jenkins
